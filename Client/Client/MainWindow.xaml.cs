﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Client()
        {

            try
            {
                TcpClient tcpclnt = new TcpClient();

                tcpclnt.Connect(IP.Text, Convert.ToInt32(Port.Text));// use the ipaddress as in the server program

                ConsoleGUI.Text = "Connected"+"\n";

                String str = InputString.Text;//input string from the GUI
                Stream stm = tcpclnt.GetStream();//used to send and receive data

                ASCIIEncoding asen = new ASCIIEncoding();//Represents an ASCII character encoding of Unicode characters.
                byte[] ba = asen.GetBytes(str);//ba is now series of bytes that represent str (input string)
                ConsoleGUI.Text=ConsoleGUI.Text+"\n"+"Transmitting....."+"\n";

                stm.Write(ba, 0, ba.Length);//send
                

                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);//read a reciver from the server

                for (int i = 0; i < k; i++)
                    ConsoleGUI.Text=ConsoleGUI.Text+Convert.ToChar(bb[i]);//output to the Console GUI the massege from the server


                tcpclnt.Close();
            }

            catch (Exception e)
            {
                ConsoleGUI.Text = "No connection..... ";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
          Client();
        }
    }

    
}
